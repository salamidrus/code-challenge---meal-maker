const menu = {
    _courses: {
      appetizers: [],
      mains: [],
      desserts: [],
    },
    get appetizers() {
      return this._courses.appetizers;
    },
    get mains() {
      return this._courses.mains;
    },
    
    get desserts() {
      return this._courses.desserts;
    },
    set appetizers(appetizers) {
      this._courses.appetizers = appetizers;
    },
    set mains(mains) {
      this._courses.mains = mains;
    },
    set desserts(desserts) {
      this._courses.desserts = desserts;
    },
    get courses() {
        return {
    appetizers: this.appetizers, 
    mains: this.mains, 
    desserts: this.desserts,
  };
  },
      addDishToCourse(courseName, dishName, dishPrice) {
      const dish = {
        name: dishName,
        price: dishPrice,
      };
    return this._courses[courseName].push(dish); 
    },
       getRandomDishFromCourse(courseName){
      const dishes = this._courses[courseName];
      const randomIndex = Math.floor(Math.random() * dishes.length);
      return dishes[randomIndex];
    },
    generateRandomMeal() {
      const appetizers = this.getRandomDishFromCourse('appetizers');
        const mains = this.getRandomDishFromCourse('mains');
      const desserts = this.getRandomDishFromCourse('desserts');
      const totalPrice = appetizers.price + mains.price + desserts.price;
      return `Your meals is ${appetizers.name}, ${mains.name}, ${desserts.name}, and the total price is ${totalPrice}.`;
      
    }
  };
  
  menu.addDishToCourse('appetizers', 'Pempek', 5);
  menu.addDishToCourse('appetizers', 'Siomay', 4);
  menu.addDishToCourse('appetizers', 'Batagor', 3);
  
  menu.addDishToCourse('mains', 'Bakso', 6);
  menu.addDishToCourse('mains', 'Lontong Sayur', 8);
  menu.addDishToCourse('mains', 'Nasi Padang', 9);
  
  menu.addDishToCourse('desserts', 'Macha Ice Cream', 7);
  menu.addDishToCourse('desserts', 'Chocolate Ice Cream', 6);
  menu.addDishToCourse('desserts', 'Strawberry Ice Cream', 5);
  
  const meal = menu.generateRandomMeal();
  
  console.log(meal);